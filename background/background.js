chrome.runtime.onInstalled.addListener(() => {
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
      chrome.declarativeContent.onPageChanged.addRules([{
        conditions: [new chrome.declarativeContent.PageStateMatcher({
          pageUrl: {hostEquals: 'www.youtube.com'},
        })
        ],
            actions: [new chrome.declarativeContent.ShowPageAction()]
      }]);
    });
});


function handlePageIconChangeByTab(tabUrl, tabId) {
  if(tabUrl.includes("https://www.youtube.com/")) {
    chrome.storage.local.get("transcriptOwnerTab", data => {
      const transcriptOwnerTabId = data.transcriptOwnerTab?.transcriptOwnerTabId;
      const transcriptOwnerTabUrl = data.transcriptOwnerTab?.transcriptOwnerTabUrl;
      if(transcriptOwnerTabId && transcriptOwnerTabUrl) {
        if(transcriptOwnerTabUrl === tabUrl) {
          setPageIcon("green", tabId);
        } else {
          setPageIcon("yellow", tabId);
        }
      }
    })
  }
}

// when you switch to some other tab
chrome.tabs.onActivated.addListener(activeInfo => {
  const tabId = activeInfo.tabId;
  chrome.tabs.get(tabId, tab => {
    const tabUrl = tab.url;
    handlePageIconChangeByTab(tabUrl, tabId);
  })
});

// when you refresh the same tab
chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
  const tabUrl = tab.url;
  handlePageIconChangeByTab(tabUrl, tabId);
});