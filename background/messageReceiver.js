chrome.runtime.onMessage.addListener((msg, sender, sendResponse) => {
    if(msg === 'transcript__downloaded') {
        
        chrome.storage.local.set({ transcriptOwnerTab: {
            transcriptOwnerTabId: sender.tab.id, 
            transcriptOwnerTabUrl: sender.tab.url
        }}, () => {
            // console.log(`transcriptOwnerTabId set to ${sender.tab.id}`)
        });

        chrome.tabs.query({active:true, currentWindow: true}, tabs => {
            var tabId = tabs[0].id;
            setPageIcon ("green", tabId);
        });
    }
});