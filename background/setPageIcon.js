function setPageIcon (pathColor, tabId) {
    let path = {
        "16": `assets/icon16_${pathColor}.png`,
        "32": `assets/icon32_${pathColor}.png`,
        "48": `assets/icon48_${pathColor}.png`,
        "64": `assets/icon64_${pathColor}.png`
    }
    chrome.pageAction.setIcon({
        path,
        tabId
    })
}