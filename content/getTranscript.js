function getTranscript(sendResponse) {
  chrome.storage.local.set({loading: true}, () => {
    // console.log("loading set to true in storage...")
  })
  const urlParams = new URLSearchParams(document.location.search.substring(1));
  const v_id = urlParams.get('v');  
  const xhr = new XMLHttpRequest();
  xhr.open('GET', `https://c0i44pv7ka.execute-api.ap-south-1.amazonaws.com/dev/${v_id}`);
  xhr.send();
  xhr.onreadystatechange = (e) => {
    if(xhr.readyState === XMLHttpRequest.DONE) {
      const {rawTranscriptData, transcriptData, message} = JSON.parse(xhr.responseText);
      let transcript = transcriptData;
      // call youtube api instead to know if the video has no caption in advance to save one call to our aws api
      if(transcript === undefined) {
        transcript = {warning: message};
      }
      const video_title = document.querySelector('h1.title > yt-formatted-string').textContent;
      chrome.storage.local.set({transcript, video_title, loading: false}, () => {
        // console.log(`Transcript value is set to ${transcript}`)
        // console.log(transcript)
        // console.log(`video_title is ${video_title}`)
        // console.log('loading set to false...')
        sendResponse({ status: "transcript_downloaded", transcript })
        chrome.runtime.sendMessage("transcript__downloaded")
      });
    }
  }
}
