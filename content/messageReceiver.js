chrome.runtime.onMessage.addListener((msg, sender, sendResponse) => {
    if(msg.timeinSeconds) {
      const video_node = document.querySelector('video');
      video_node.currentTime = msg.timeinSeconds
    } else if (msg.task) {
      if(msg.task === 'start_transcript_download') {
        getTranscript(sendResponse);
      } else if (msg.task === 'check_transcript_availability') {
        const isTranscriptPresent = isTranscriptAvailable();
        chrome.storage.local.set({ isTranscriptPresent }, () => {
          // console.log(`isTranscriptPresent set to ${isTranscriptPresent}`)
        })
        sendResponse({ criteria: 'transcript_availability' });
      }
    }
    return true;
})
  