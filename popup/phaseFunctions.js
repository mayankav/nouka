function popupLoaded() {
    
    document.getElementById('no-transcript').style.display = 'none';
    document.getElementById('spinner_wrapper').style.display = 'none';
    
    chrome.storage.local.get("transcript", data => {
        const transcript = data.transcript
        if(transcript) {
            addTranscriptToPopupDOM(transcript);
        } else {
            document.getElementById('search_wrapper').style.display = 'none';
        }
    });

    chrome.storage.local.get("video_title", function(data) {
        const video_title = data.video_title;
        document.getElementById('v_title').textContent = video_title
    });

    chrome.storage.local.get("loading", data => {
        if(data.loading) {
            document.getElementById('btn_wrapper').style.display = 'none';
            document.getElementById('search_wrapper').style.display = 'none';
            document.getElementById('spinner_wrapper').style.display = 'block';
        }
    })
}

function downloadTranscriptIfPresent() {
    chrome.storage.local.get('isTranscriptPresent', data => {
        const isTranscriptAvailable = data.isTranscriptPresent;
        if(isTranscriptAvailable) {
            sendMessageToContent({task: 'start_transcript_download'}, responseMsg => {
                transcriptDownloaded();
            });
            downloadTranscriptStarted();
        } else {
            document.getElementById('no-transcript').style.display = 'block';
        }
    })
}

function downloadTranscriptStarted() {
    document.getElementById('spinner_wrapper').style.display = 'block';
    document.getElementById('btn_wrapper').style.display = 'none';
    document.getElementById('search_wrapper').style.display = 'none';
}

function transcriptDownloaded() {
    chrome.storage.local.get("transcript", function(data) {
        const transcript = data.transcript;
        if(transcript) {
            addTranscriptToPopupDOM(transcript);
        }
    });
    chrome.storage.local.get("video_title", function(data) {
        const video_title = data.video_title;
        document.getElementById('v_title').textContent = video_title
    });

    document.getElementById('spinner_wrapper').style.display = 'none';
    document.getElementById('btn_wrapper').style.display = 'block';
    document.getElementById('search_wrapper').style.display = 'block';
}