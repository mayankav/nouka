
function addTranscriptToPopupDOM(transcript) {
    const listNode = document.getElementById('captionList');
    listNode.innerHTML = "";
    Object.entries(transcript).forEach(([key, val]) => {
        const listItem = document.createElement('div');
        listItem.setAttribute('class', 'captionItem')
        listItem.onclick = function() {
            sendMessageToContent({timeinSeconds: key});
        }
        const listItemText = document.createTextNode(val);
        listItem.appendChild(listItemText);
        listNode.appendChild(listItem);
    })
}