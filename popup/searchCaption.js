const debouncedSearchCaption = debounce(searchCaption, 300);

document.getElementById("search").addEventListener("input", debouncedSearchCaption);

function searchCaption () {
    chrome.storage.local.get("transcript", function(data) {
        const transcript = data.transcript;
        const searchText = document.getElementById("search").value;
        const filteredTranscriptEntries = Object.entries(transcript).filter(([key, val]) => {
            return val.toLowerCase().includes(searchText.toLowerCase())
        });
        const filteredTranscript = Object.fromEntries(filteredTranscriptEntries);
        addTranscriptToPopupDOM(filteredTranscript);
    });
}

function debounce (fn, delay) {
    let timer;
    return () => {
        clearTimeout(timer);
        timer = setTimeout(fn, delay);
    }
}


