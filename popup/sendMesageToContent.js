function sendMessageToContent(msg, callback) {
    const params = {
        active: true,
        currentWindow: true
    }
    chrome.tabs.query(params, (tabs) => {
            chrome.tabs.sendMessage(tabs[0].id, msg, {}, callback);
    })
}